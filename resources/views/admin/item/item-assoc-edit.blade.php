@extends('layouts.admin')

@section('content')

    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
        </ul>
    </div>

    <?= Form::open(['name'  => 'skillTitleForm',
            'url'           => '/admin/item-assoc-update/'
                    . $data['holderItem'] . '/' . $data['relatedItem'],
            'files'         => false,
            'id'            => '']);
    ?>

    <table id="skill_title_container">
        <tr>
            <th><?= Lang::get('app.skill_label') ?></th>
            @foreach($data['relatedItems'] as $relatedItem)
                <th>{{ $relatedItem->$data['relatedItemLabelField'] }}</th>
            @endforeach
        </tr>

        @foreach($data['itemHolderWithRelated'] as $holder)
            <tr>
                <td>{{ $holder[$data['holderItemLabelField']] }}</td>
                @for($i=0; $i < $data['qtyRelatedItems']; $i++)
                    <td>
                        <?= Form::checkbox('holderItems['
                                . $holder['id'] . '][' . $data['relatedItems'][$i]->id .']',
                                call_user_func([$isItemRelated[0], $isItemRelated[1]],
                                        $data['relatedItems'][$i]->id, $holder,
                                        $data['holderItem'], $data['relatedItem']
                                ),
                               call_user_func([$isItemRelated[0], $isItemRelated[1]],
                                       $data['relatedItems'][$i]->id, $holder,
                                       $data['holderItem'], $data['relatedItem']
                               ));
                        ?>
                    </td>
                @endfor
            </tr>
        @endforeach
    </table>

    <div>
        <?= Form::submit(Lang::get('app.update'),
                ['id' => '',
                        'class' => 'btn btn-primary btn-lg'
                ]);
        ?>
    </div>

    <?= Form::close(); ?>
@stop