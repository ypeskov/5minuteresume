<h2 class="skills"><?= mb_ucfirst(Lang::get('app.technical_skills')) ?></h2>


    @for($i=0; $i < count($data['skills']); $i++)
        <div class="skill_item_holder" data-skill_id="{{ $data['skills'][$i]['id'] }}">
            <h3 class="skill_label">{{ $data['skills'][$i]['skill'] }}</h3>

            <div class="skill_item skill_tag_display options-list" data-skill_id="{{ $data['skills'][$i]['id'] }}">
                <ul class="skill_tags" data-skill_id="{{ $data['skills'][$i]['id'] }}"></ul>
            </div>

            <div class="skill_tag_holder">
                <span class="visible_tags dynamic_tags_holder"></span>
                <span class="skill_more_tags">
                    <a href="#" class="more_tags hidden"><?= Lang::get('app.more_tags') ?></a>
                </span>
            </div>

            <div class="hidden more_tags_holder dynamic_tags_holder"></div>

            <span class="hidden skill_tag_template"><a href="#"></a></span>
        </div>
    @endfor