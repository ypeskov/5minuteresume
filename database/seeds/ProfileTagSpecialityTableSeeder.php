<?php

use Illuminate\Database\Seeder;
use App\Speciality;

class ProfileTagSpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Speciality::find(1)->profileTags()->sync([1, 2, 3, 5, ]);
        Speciality::find(3)->profileTags()->sync([4, 5, ]);
    }
}
