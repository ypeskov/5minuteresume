@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
        </ul>
    </div>

    <?= Form::open(['name'  => 'editItemsForm',
            'url'           => '/admin/update-item/' . $data['item'],
            'files'         => false,
            'id'            => '']);
    ?>

    <table class="items_container">
        <tr>
            <th><?= mb_ucfirst(Lang::get('app.id')) ?></th>
            <th><?= mb_ucfirst(Lang::get('app.current_label')) ?></th>
            <th><?= mb_ucfirst(Lang::get('app.new_label')) ?></th>
        </tr>
        @foreach($data['items'] as $item)
            <tr>
                <td>{{ $item['id'] }}</td>
                <td>{{ $item[$data['fieldLabel']] }}</td>
                <td>
                    <?= Form::text('items['. $item['id'] . ']',
                            '',
                            ['class' => 'form-control', ]);
                    ?>
                </td>
            </tr>
        @endforeach
    </table>

    <div>
        <?= Form::submit(mb_ucfirst(Lang::get('app.update')),
                ['id' => '',
                        'class' => 'btn btn-primary btn-lg'
                ]);
        ?>
    </div>

    <?= Form::close(); ?>
@stop