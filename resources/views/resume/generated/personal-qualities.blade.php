@if ( sizeof($resume['quality_tags']) > 0 )

    <h2><?= mb_ucfirst(Lang::get('app.personal_qualities')) ?></h2>

    <ul>
        @foreach($resume['quality_tags'] as $tag)
            <li>{{ $tag }}</li>
        @endforeach
    </ul>

@endif