@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <ul>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/">Dashboard</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/speciality">Specialities</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/tag">Skill Tags</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/profile-tag">Profile Tags</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/personal-quality">Personal qualities</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/responsibility">Responsibilities</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/skill">Skills</a>
                </li>
                <li>
                    <a href="/{{ $data['adminPrefix'] }}/list-resumes">List resumes</a>
                </li>
            </ul>
        </div>
    </div>
@stop