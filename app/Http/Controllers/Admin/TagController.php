<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\Tag;


class TagController extends BaseItemController
{
    public function indexAction()
    {
        $data['tags'] = Tag::all()->toArray();

        return view('admin.tag.tag-index', ['data' => $data]);
    }
}