@extends('layouts.pdf')

@section('content')

    <header id="header">
        @include('resume.generated.header')

    </header>


    <div id="main">
        <section>
            @include('resume.generated.personal-info')
        </section>

        <section>
            @include('resume.generated.profile-tags')
        </section>

        <section>
            @include('resume.generated.skill-tags')
        </section>

        <section>
            @include('resume.generated.experience')

        </section>

        <section>
            @include('resume.generated.education')

        </section>

        <section>
            @include('resume.generated.personal-qualities')
        </section>

    </div>

    @if ( $isHtml )

        <footer id="footer">
            @include('resume.generated.footer')
        </footer>

        <div class="send_resume_link">
            <a id="send_resume"
               href="#"
               data-id="<?= $resume['id'] ?>"
                    ><?= mb_ucfirst(Lang::get('app.send_cv_to_email')) ?></a>

            <a href="/resume/pdf/<?= $resume['id'] ?>"
               target="_blank"><?= mb_ucfirst(Lang::get('app.pdf_preview')) ?>
            </a>
        </div>


        <div class="popup hidden">
            <a id="close_sent_popup" href="#" class="close">close</a>
            <figure>
                <img src="/images/ico01.png" alt="image description"/>
            </figure>
            <p>Success! <br/>Your resume is sent to your email address.</p>
            <span class="bottom-text">
                <a href="#"><span class="red">5min</span>resume.com</a>
            </span>
        </div>

        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/js/resume-view.js"></script>
    @endif

@stop