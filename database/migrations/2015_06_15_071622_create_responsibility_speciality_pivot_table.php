<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsibilitySpecialityPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsibility_speciality', function(Blueprint $table) {
            $table->integer('responsibility_id')->unsigned()->index();
            $table->foreign('responsibility_id')->references('id')->on('responsibilities')->onDelete('cascade');
            $table->integer('speciality_id')->unsigned()->index();
            $table->foreign('speciality_id')->references('id')->on('specialities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('responsibility_speciality');
    }
}
