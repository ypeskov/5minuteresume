@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/skill">Edit skills</a></li>
            <li><a href="/admin/edit-item-assoc/skill/speciality">Skill2Speciality</a></li>
            <li><a href="/admin/new-item/skill">New Skill</a></li>
        </ul>

    </div>

    <hr style="clear: left;" />

    <div>
        <table class="items_container">
        @foreach($data['skills'] as $skill)
            <tr>
                <td>{{ $skill['id'] }}</td>
                <td>{{ $skill['skill'] }}</td>
                <td>
                    <a href="/admin/item-delete/skill/{{ $skill['id'] }}">{{ Lang::get('app.delete') }}</a>
                </td>
            </tr>
        @endforeach
        </table>
    </div>
@stop