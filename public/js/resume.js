function Resume(allSpecialities, allTags)
{
    this.apiPrefixUrl = '/api/v1.0';

    this.qtyVisibleTags = qtyVisibleTags;
    this.qtyTagsToAdd   = qtyTagsToAdd;

    this.allSpecialities = allSpecialities;
    this.allTags = allTags;

    this.specialityWrapper = '#speciality_wrapper';
    this.qualitySelectedTagsHolder = '#personal_qualities';
    this.profileTagsHolder = '#profile_tags';

    this.availableTags      = [];
    this.availableSkills    = {};
    this.availableResponsibilityTags = [];
    this.availableProfileTags = [];
}

/**
 * return full object from array by parameter name
 *
 * @param label
 * @returns {*}
 */
Resume.prototype.getFullObjectByParameter = function(arrSrc, paramVal, paramName) {
    for(var i=0; i < arrSrc.length; i++) {
        if ( arrSrc[i][paramName] === paramVal ) {
            return arrSrc[i];
        }
    }
};

/**
 * Black vodoo magic. Better not to touch it.
 */
Resume.prototype.updateSkillTags = function() {
    var _self = this;

    //clean up all skill tags first
    $('ul.skill_tags').each(function(index, val) {
        $(val)
            .next('div.skill_tag_holder')
            .children('span.visible_tags')
            //.not('span.visible_tags, span.skill_more_tags')
            .empty();
        $(val).tagit('removeAll');
    });

    var skillWrapper = $('.skill_item').not('.hidden');

    skillWrapper.each(function(index, val) {
        var elem = $(val);//.find('ul.skill_tags');  //trash from old html
        var skillId = $(val).attr('data-skill_id');

        var tags = _self.availableTags[skillId];
        if ( tags == undefined ) {
            tags = [];
        }

        var moreTagsHolder = elem.siblings('div.more_tags_holder');

        var tagHolder = elem.next('div.skill_tag_holder').find('span.visible_tags');

        for(var i=0; i < tags.length; i++) {

            var template =  tagHolder.parent()
                .siblings('span.skill_tag_template')
                .clone()
                .removeClass();

            var text = tags[i]['tag_name'];
            template.children('a').html(text);

            if ( i < _self.qtyVisibleTags ) {
                if ( i < (_self.qtyVisibleTags - 1) ) {
                    template.append('<span>&nbsp;</span>');
                }

                tagHolder.append(template);
            } else {
                moreTagsHolder.append(template);
            }


            template.on('click', function(event) {
                var curTar = $(event.currentTarget);
                var tag = curTar.children('a').text();

                curTar.parents('div.skill_tag_holder')
                    .prev('div.skill_tag_display')
                    .children('ul.skill_tags')
                    .tagit('createTag',tag);

                return false;
            });
        }

        if ( moreTagsHolder.children().length > 0 ) {
            tagHolder.next().find('a.more_tags').removeClass('hidden');
        }

    });
};

Resume.prototype.updateAvailableSkills = function(specialityIds) {
    var _self = this;

    $.ajax( {
        url: _self.apiPrefixUrl + '/skills/filtered?specialityId=' + specialityIds.join(','),
        type: 'GET',
        success: function(data) {
            _self.availableSkills   = data['skills'];
            _self.availableTags     = data['tags'];
            _self.availableResponsibilityTags = data['availableResponsibilityTags'];
            _self.availableProfileTags = data.availableProfileTags;

            $('.skill_item_holder').addClass('hidden');

            //and now loop all skills and display available
            $('.skill_item_holder').map(function(index, value){
                var elem = $(value);
                var id = parseInt(elem.attr('data-skill_id'));

                for(var i in _self.availableSkills) {
                    if ( _self.availableSkills[i]['id'] == id ) {
                        elem.removeClass('hidden');
                    }
                }
            });


            // update available tags for each skill
            $('.dynamic_tags_holder').empty();
            _self.updateSkillTags();
            _self.updateResponsibilityTags();
            _self.updateProfileTags();
        }
    });
};
count = 0;
Resume.prototype.getSelectedSpecialities = function() {
    var _self = this;

    var specialities = [];

    $(_self.specialityWrapper + ' li > span').map(function(index, value) {
        var spec = $(value).text();
        specialities.push(
            _self.getFullObjectByParameter(_self.allSpecialities, spec, 'speciality')['id']);
    });

    return specialities;
};

Resume.prototype.addTaggerEvent = function() {
    $('a.responsibility_tag').each(function(ind, val) {
        $(val).off();
        $(val).on('click', function(event) {
            var tag = $(event.currentTarget);
            tag
                .parent()
                .parent()
                .parent()
                .prev()
                .children('ul')
                .tagit('createTag', tag.text());

            return false;
        });
    });
};

Resume.prototype.updateResponsibilityTags = function() {
    var _self = this;

    var tagsPlaceholders = $('div.available_responsibilities_tags').children('span.visible_tags');
    tagsPlaceholders.empty();

    var tags = _self.availableResponsibilityTags;

    var newTags = [];
    var newHiddenTags = [];

    for(var i=0; i < tags.length; i++) {
        var elemWrapper = document.createElement('span');
        var aElem = $(document.createElement('span'))
            .addClass('rowly_tag_item');

        aElem
            .append(document.createElement('a'));
        aElem
            .children('a')
            .text(tags[i].responsibility_tag)
            .attr('href', '#')
            .addClass('responsibility_tag');

        elemWrapper = $(elemWrapper).append(aElem);

        if ( i < _self.qtyVisibleTags ) {
            newTags.push(elemWrapper.html());
        } else {
            newHiddenTags.push($(elemWrapper).html());
        }

    }

    var tagsHTML = newTags.join(' ');
    var newHiddenTagsHTML = newHiddenTags.join(' ');

    tagsPlaceholders.each(function(index, val) {
        var moreTagsHolder = $(val).parent().next('div.more_tags_holder');

        $(val).html(tagsHTML);
        moreTagsHolder.html(newHiddenTagsHTML);


        //make more label visible if required
        if ( moreTagsHolder.children().length > 0 ) {
            $(val).next('span').children('a.more_tags').removeClass('hidden');
        } else {
            $(val).next('span').children('a.more_tags').addClass('hidden');
        }
    });

    _self.addTaggerEvent();
};

Resume.prototype.updateProfileTags = function() {
    var _self = this;

    var selectedTagsHolder = $(_self.profileTagsHolder);
    selectedTagsHolder.tagit('removeAll');

    var tags = _self.availableProfileTags;
    var availableTagHolder = $('#available_profile_tags .visible_tags');
    var hiddenTagsHolder = availableTagHolder.parent().next();

    availableTagHolder.html('');

    for(var i in tags) {
        var newTag = $('#profile_tag_template')
            .clone()
            .removeAttr('id')
            .removeClass('hidden');
        newTag
            .children('a')
            .text(tags[i].label);


        if ( i < _self.qtyVisibleTags ) {
            availableTagHolder.append(newTag);

            if ( i < (_self.qtyVisibleTags-1) ) {
                availableTagHolder.append('  ');
            }

        } else {
            hiddenTagsHolder.append(newTag);
        }

        newTag.on('click', function(event) {
            var clickedTag = $(event.currentTarget);
            selectedTagsHolder.tagit('createTag', clickedTag.text());

            return false;
        });

    }

    if ( $(hiddenTagsHolder).children().length > 0 ) {
        availableTagHolder.parent().find('a.more_tags').removeClass('hidden');
    }
};

/**
 * Add speciality to enabled only if it is not in the list yet.
 * @param elem
 */
Resume.prototype.addSpeciality = function(elem, tagHolder) {
    var _self = this;

    var elem = $(elem);
    var specialityLabel = elem.attr('data-specialityLabel');

    var tagHolder = $(tagHolder);

    tagHolder.tagit('removeAll');
    tagHolder.tagit('createTag', specialityLabel);
    $('.dynamic_tags_holder').empty();

    var specialities = _self.getSelectedSpecialities();
    _self.updateAvailableSkills(specialities);
};

Resume.prototype.addProject = function(projectForm) {
    var _self = this;

    var newProject = projectForm.clone();
    newProject.find('input').val('');
    newProject.find('li').remove();
    newProject.find('ul.responsibilities_tags').tagit();

    projectForm.after(newProject);

    _self.addTaggerEvent();

    $(newProject).find('a.more_tags').on('click', function(event) {
        _self.addMoreTags(event);

        return false;
    });
};

Resume.prototype.addEducation = function(educationWrapper) {
    var _self = this;

    var newEducation = educationWrapper.clone();
    newEducation.find('input').val('');
    educationWrapper.parent().append(newEducation);

    //add click event listener for adding more educations
    newEducation.find('.add_education').on('click', function(event) {
        _self.clickedAddEducation(event);
        return false;
    });
};

Resume.prototype.addQualityTag= function(tagElement) {
    var _self = this;

    var tag = tagElement.text();

    $(_self.qualitySelectedTagsHolder).tagit('createTag', tag);
};

Resume.prototype.storeResume = function(resumeData) {
    var _self = this;

    var token = document.forms[0]._token.value;

    $.ajax({
        url:    _self.apiPrefixUrl + '/resume',
        method: 'POST',
        data:   {
            _token:     token,
            resumeData: resumeData
        },
        success: function(data) {
            if ( data.error === false ) {
                window.location = '/resume/html/' + data.data.resume.id;
            }
        }
    });
};

Resume.prototype.collectResponsibilitiesTags = function() {
    var projectWrappers = $('div.experience_item');

    var projects = [];
    projectWrappers.each(function(ind, val) {
        var wrapper = $(val);

        var projectData = {};
        projectData.company_name = wrapper.find('input.project_company_name').val();
        projectData.city = wrapper.find('input.project_city').val();
        projectData.month_start = wrapper.find('input.project_month_start').val();
        projectData.year_start = wrapper.find('input.project_year_start').val();
        projectData.month_end = wrapper.find('input.project_month_end').val();
        projectData.year_end = wrapper.find('input.project_year_end').val();
        projectData.link = wrapper.find('input.project_link').val();
        projectData.description = wrapper.find('textarea.project_description').val();
        projectData.responsibilities_tags =
            wrapper.find('.responsibilities_tags').tagit('assignedTags');

        projects.push(projectData);
    });

    return projects;
};

Resume.prototype.collectPersonalInfo = function() {
    var personalInfo = {};

    personalInfo['firstName']         = $('#first_name').val();
    personalInfo['lastName']          = $('#last_name').val();
    personalInfo['city']              = $('#city').val();
    personalInfo['country']           = $('#country').val();
    personalInfo['email']             = $('#email').val();
    personalInfo['contactDetails']    = $('#contact_details').val();

    return personalInfo;
};

/**
 * Store selected specialities here to be saved.
 *
 * @type {Array}
 */
Resume.prototype.collectSpecialities = function() {
    var _self = this;
    var selectedSpecialities = [];

    $(_self.specialityWrapper + ' li > span').map(function(index, value) {
        var speciality =
            _self.getFullObjectByParameter(_self.allSpecialities,
                $(value).text(), 'speciality');

        selectedSpecialities
            .push({
                'id':           speciality['id'],
                'speciality':   speciality['speciality']
            });
    });

    return selectedSpecialities;
};

Resume.prototype.collectSkillTags = function() {
    var _self = this;
    var skillTags = {};

    $('.skill_item').not('.hidden').each(function(index, value) {
        var container = $(value);
        var skillLabel = container.prev('h3.skill_label').text();
        var skillId = container.attr('data-skill_id');

        //if property for this skill doesn't exist yet create it
        if ( !skillTags[skillId] ) {
            skillTags[skillLabel] = [];
        }

        $(value).find('li.tagit-choice span.tagit-label').each(function(index, val) {
            var tagLabel = $(val).text();
            var tag =
                _self.getFullObjectByParameter(_self.allTags, tagLabel, 'tag_name');

            skillTags[skillLabel].push({
                tagId: tag.id,
                tagName: tagLabel
            });
        });
    });

    return skillTags;
};

Resume.prototype.collectEducation = function() {
    var educations = [];
    $('.education_item').each(function(ind, val) {
        educations.push({
            university: $(val).find('.education_university').val(),
            city:       $(val).find('.education_city').val(),
            year:       $(val).find('.education_year').val()
        });
    });

    return educations;
};

Resume.prototype.collectPersonalQualities = function() {
    var qualityTags = [];
        $('#personal_qualities').find('.tagit-label').each(function(ind, val) {
            qualityTags.push($(val).text());
        });

    return qualityTags;
};

Resume.prototype.collectProfileTags = function() {
    var tags = $('#profile_tags').tagit('assignedTags');

    return tags;
};

Resume.prototype.makeVerification = function() {
    var email = document.getElementById('email').value;

    var isValid = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/.test(email);

    //go page up and show error
    if ( !isValid ) {
        $('#email_error').removeClass('hidden');
        $('#email').parent().parent().addClass('error');

        var intervalId = window.setInterval(function() {
            window.scrollTo(0, window.pageYOffset - 50);

            if ( window.pageYOffset === 0 ) {
                clearInterval(intervalId);
            }
        }, 5);
    }


    return isValid;
};

Resume.prototype.processBeforeSubmit = function() {
    var _self = this;

    var resumeData = {};

    if ( !_self.makeVerification() ) {
        return false;
    }

    resumeData.personalInfo         = _self.collectPersonalInfo();
    resumeData.resumeSpecialities   = _self.collectSpecialities();
    resumeData.skillTags            = _self.collectSkillTags();
    console.log(resumeData.skillTags);
    resumeData.projects             = _self.collectResponsibilitiesTags();
    resumeData.educations           = _self.collectEducation();
    resumeData.quality_tags         = _self.collectPersonalQualities();
    resumeData.profile_tags         = _self.collectProfileTags();

    _self.storeResume(resumeData);
};

Resume.prototype.clickedAddEducation = function(event) {
    var _self = this;

    var addLink = $(event.currentTarget);

    _self.addEducation(addLink.parents('.education_item'));
    addLink.remove();

    return false;
};

Resume.prototype.addMoreTags = function(event) {
    var _self = this;

    var moreLink = $(event.currentTarget);
    var moreTagsHolder = moreLink.parent().parent().next();
    var visibleTagsHolder = moreLink.parent().prev();

    for(var i=0; i < _self.qtyTagsToAdd; i++ ) {
        var children = moreTagsHolder.children();
        if (children.length > 0) {
            visibleTagsHolder.append(' ');
            $(children[0]).appendTo(visibleTagsHolder);
        } else {
            moreLink.addClass('hidden');
        }
    }

    if ( moreTagsHolder.children().length === 0 ) {
        moreLink.addClass('hidden');
    }
};

$(document).ready(function() {

    //use global variable generated on server side allSpecialities
    var resume = new Resume(allSpecialities, allTags);

    $('#resume_speciality').tagit({
        afterTagRemoved: function(event, ui) {
            var specialities = resume.getSelectedSpecialities();
            resume.updateAvailableSkills(specialities);
        }
    });

    $('.skill_tags').tagit();
    $('.responsibilities_tags').tagit();

    $('#profile_tags').tagit();

    $('#email').on('focus', function() {
        $('#email_error').addClass('hidden');
        $('#email').parent().parent().removeClass('error');
    });

    $('.speciality_selector').on('click', function(event) {
        resume.addSpeciality($(event.currentTarget), '#resume_speciality');

        return false;
    });

    $('.add_project').on('click', function(event) {
        console.log($(event.currentTarget));
        resume.addProject($(event.currentTarget)
                            .parent()
                            .parent()
                            .prev('div.experience_item'));

        return false;
    });

    $('.add_education').on('click', function(event) {
        resume.clickedAddEducation(event);
        return false;
    });

    $('#personal_qualities').tagit();

    $('#qualities_container')
        .children('span.visible_tags')
        .children('a')
        .on('click', function(event) {
            resume.addQualityTag($(event.currentTarget));

            return false;
    });

    $('#qualities_container')
        .next('div.more_tags_holder')
        .children('a')
        .on('click', function(event) {
            resume.addQualityTag($(event.currentTarget));

            return false;
        });

    $('#submit_resume').on('click', function(event) {
        resume.processBeforeSubmit(allSpecialities);

        return false;
    });

    $('ul.tagit input').attr('readonly', 'true');

    $('.more_tags').on('click', function(event) {
        resume.addMoreTags(event);

        return false;
    });
});
