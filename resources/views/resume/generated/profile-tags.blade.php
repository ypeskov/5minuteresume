@if ( sizeof($resume['profile_tags']) > 0 )

    <h2><?= mb_ucfirst(Lang::get('app.profile')) ?></h2>

    @for($i=0; $i < sizeof($resume['profile_tags']); $i++)
        <p class="profile_tag_item">{{ $resume['profile_tags'][$i] }}</p>
    @endfor

@endif