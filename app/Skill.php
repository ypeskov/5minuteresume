<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    public function specialities()
    {
        return $this->belongsToMany('App\Speciality');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}
