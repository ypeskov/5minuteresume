@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/responsibility">Edit Responsibility Tags</a></li>
            <li><a href="/admin/edit-item-assoc/responsibility/speciality">Responsibility2Speciality</a></li>
            <li><a href="/admin/new-item/responsibility">New Responsibility Tag</a></li>
        </ul>

    </div>

    <hr style="clear: left;" />

    <div>
        <table class="items_container">
        @foreach($data['tags'] as $tag)
            <tr>
                <td>{{ $tag['id'] }}</td>
                <td>{{ $tag['responsibility_tag'] }}</td>
                <td>
                    <a href="/admin/item-delete/responsibility/{{ $tag['id'] }}">
                        {{ Lang::get('app.delete') }}
                    </a>
                </td>
            </tr>
        @endforeach
        </table>
    </div>
@stop