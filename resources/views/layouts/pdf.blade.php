<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>5 Minutes Resume</title>

    <style><?php include('css/all-pdf.css'); ?></style>
</head>

<body>
    <div id="wrapper">
        @yield('content')
    </div>
</body>
</html>