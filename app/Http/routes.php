<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',[
    'as'    => 'cvForm',
    'uses'  => 'CvFormController@showForm',
]);

Route::group(['prefix' => 'resume', ], function() {
    Route::get('/html/{id}', [
        'as'    => 'resumeHTML',
        'uses'  => 'CvGeneratorController@htmlAction',
    ])->where(['id' => '[0-9]+']);

    Route::get('/pdf/{id}', [
        'as'    => 'resumePDF',
        'uses'  => 'CvGeneratorController@pdfAction',
    ])->where(['id' => '[0-9]+']);

    Route::get('/send/{id}', [
        'as'    => 'sendResume',
        'uses'  => 'CvGeneratorController@sendPdfAction',
    ])->where(['id' => '[0-9]+']);
});

Route::group(['prefix' => 'api/v1.0'], function() {

    Route::get('/skills/filtered', [
        'as'    => 'filteredTags',
        'uses'  => 'ResumeApiController@getFilteredSkillsAction',
    ]);

    Route::post('/resume', [
        'as'    => 'newResume',
        'uses'  => 'ResumeApiController@newResumeAction',
    ]);

    Route::get('/resume/send/{id}', [
        'as'    => 'newResume',
        'uses'  => 'ResumeApiController@sendResumeAction',
    ])->where(['id' => '[0-9]+']);
});

Route::group(['prefix' => 'admin'], function() {

    Route::get('/', [
        'as' => 'adminDashboard',
        'uses' => 'Admin\AdminController@dashboardAction',
    ]);

    Route::get('/list-resumes', [
        'as' => 'listResumesAction',
        'uses' => 'Admin\AdminController@resumeListAction',
    ]);

    Route::get('/delete-resume/{id}', [
        'as' => 'deleteResumeAction',
        'uses' => 'Admin\AdminController@deleteResumeAction',
    ])->where(['id' => '[0-9]+']);

    Route::get('/new-item/{item}', [
        'as' => 'newItem',
        'uses' => 'Admin\BaseItemController@newItemAction'
    ]);

    Route::post('/add-item/{item}', [
        'as' => 'addItem',
        'uses' => 'Admin\BaseItemController@addItemAction'
    ]);

    Route::get('/item-delete/{item}/{itemId}', [
        'as' => 'deleteItem',
        'uses' => 'Admin\BaseItemController@deleteItemAction',
    ])->where(['itemId' => '[0-9]+',]);

    Route::get('/edit-item-assoc/{item1}/{item2}', [
        'as' => 'assocItemEdit',
        'uses' => 'Admin\BaseItemController@assocItemEditAction',
    ]);

    Route::post('/item-assoc-update/{item1}/{item2}', [
        'as' => 'assocItemEdit',
        'uses' => 'Admin\BaseItemController@assocItemUpdateAction',
    ]);

    Route::get('/edit-item/{item}', [
        'as' => 'itemEdit',
        'uses' => 'Admin\BaseItemController@editItemAction',
    ]);

    Route::post('/update-item/{item}', [
        'as' => 'itemUpdate',
        'uses' => 'Admin\BaseItemController@updateItemAction',
    ]);

    //-------------------- Item Index Routes ---------------------------

    Route::get('/speciality', [
        'as' => 'adminSpeciality',
        'uses' => 'Admin\SpecialityController@indexAction',
    ]);

    Route::get('/tag', [
        'as' => 'adminTag',
        'uses' => 'Admin\TagController@indexAction',
    ]);

    Route::get('/profile-tag', [
        'as' => 'adminProfile-tag',
        'uses' => 'Admin\ProfileTagController@indexAction',
    ]);

    Route::get('/skill', [
        'as' => 'adminSkill',
        'uses' => 'Admin\SkillController@indexAction',
    ]);

    Route::get('/personal-quality', [
        'as' => 'adminPersonal-quality',
        'uses' => 'Admin\PersonalQualityController@indexAction',
    ]);

    Route::get('/responsibility', [
        'as' => 'adminResponsibility',
        'uses' => 'Admin\ResponsibilityController@indexAction',
    ]);
});

