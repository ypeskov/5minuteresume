<?php

use Illuminate\Database\Seeder;

use App\Speciality;
use App\Tag;

class SpecialityTagTableSeeder extends Seeder
{
    public function run()
    {
        Tag::find(3)->specialities()->sync([1, 2, 3,  ]);
        Speciality::find(3)->tags()->sync([3, 4, 5, 6, ]);

        Speciality::find(1)->tags()->sync([1, 2, 3, 4, 5, 6, ]);
        Speciality::find(2)->tags()->sync([3, 4, 5, 7, ]);
    }
}

