<?php

use Illuminate\Database\Seeder;
use App\PersonalQualityTag;

class PersonalQualityTagTableSeeder extends Seeder
{
    public function run()
    {
        PersonalQualityTag::create([
            'id'    => 1,
            'quality_tag'   => 'proactive',
        ]);

        PersonalQualityTag::create([
            'id'    => 2,
            'quality_tag'   => 'attention',
        ]);

        PersonalQualityTag::create([
            'id'    => 3,
            'quality_tag'   => 'cLever',
        ]);
    }
}
