@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/profile-tag">Edit profile tags</a></li>
            <li><a href="/admin/edit-item-assoc/profile-tag/speciality">ProfileTag2Speciality</a></li>
            <li><a href="/admin/new-item/profile-tag">New Profile Tag</a></li>
        </ul>

    </div>

    <hr style="clear: left;" />

    <div>
        <table class="items_container">
        @foreach($data['tags'] as $tag)
            <tr>
                <td>{{ $tag['id'] }}</td>
                <td>{{ $tag['label'] }}</td>
                <td>
                    <a href="/admin/item-delete/profile-tag/{{ $tag['id'] }}">
                        {{ Lang::get('app.delete') }}
                    </a>
                </td>
            </tr>
        @endforeach
        </table>
    </div>
@stop