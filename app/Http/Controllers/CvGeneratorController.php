<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 2015-06-18
 * Time: 15:41
 */

namespace App\Http\Controllers;

use \Redirect;
use App\Resume\ResumeGenerator;

class CvGeneratorController extends Controller
{
    protected $resumeGenerator;

    public function __construct(ResumeGenerator $generator)
    {
        $this->resumeGenerator = $generator;
    }

    public function htmlAction($id)
    {
        return view('resume.generated-cv',
            ['resume' => $this->resumeGenerator->getPreparedResume($id),
                'isHtml'    => true,
            ]
        );
    }

    public function pdfAction($id)
    {
        if ( $this->resumeGenerator->getPdfResume($id) ) {
            //return Redirect::route('cvForm');
        } else {

        }
    }

    public function sendPdfAction($id)
    {
        $resume = $this->resumeGenerator->getPreparedResume($id);

        if ( $this->resumeGenerator->sendPdfResume($id) ) {
            return view('resume.sent-ok', ['resume' => $resume]);
        } else {
            return view('resume.sent-error');
        }
    }
}