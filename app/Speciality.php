<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }

    public function resumes()
    {
        return $this->belongsToMany('App\Resume');
    }

    public function responsibilities()
    {
        return $this->belongsToMany('App\Responsibility');
    }

    public function profileTags()
    {
        return $this->belongsToMany('App\ProfileTag');
    }
}
