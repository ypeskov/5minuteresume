<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>5 Minutes Resume Admin section</title>

    <link rel="stylesheet" href="/vendor/bootstrap-dist/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/vendor/tag-it/css/jquery.tagit.css" type="text/css">
    <link rel="stylesheet" href="/vendor/tag-it/css/tagit.ui-zendesk.css" type="text/css">
    <link rel="stylesheet" href="/vendor/jquery-ui/jquery-ui.min.css">

    <link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>

<body>
<div class="container">

    @if ( Session::has('flashMessage') )
        <div id="flashMessage">{{ Session::get('flashMessage') }}</div>
    @endif

    @if ( Session::has('flashWarning') )
        <div id="flashWarning">{{ Session::get('flashWarning') }}</div>
    @endif


    <div class="row">
        <div class="col-xs-12">
            @yield('content')
        </div>
    </div>

</div>

<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/tag-it/js/tag-it.min.js"></script>
</body>

</html>