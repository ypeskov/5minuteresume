<?php

use Illuminate\Database\Seeder;
use App\Skill;

class SkillTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::find(1)->tags()->sync([1, 7,]);
        Skill::find(4)->tags()->sync([4, 5, 6, ]);
        Skill::find(5)->tags()->sync([9, 10, 11, ]);
        Skill::find(6)->tags()->sync([2, 8, ]);
        Skill::find(3)->tags()->sync([3, ]);
    }
}

