@if ( sizeof($resume['projects']) > 0 )

    @if ( $resume['projectContainInfo'] )

        <h2><?= mb_ucfirst(Lang::get('app.professional_history')) ?></h2>

        @foreach($resume['projects'] as $project)

            <div class="position">
                <p>
                    {{ $project['company_name'] }}
                    @if ( !empty($project['company_name']) and !empty($project['city']) ),&nbsp;@endif
                    {{ $project['city'] }}
                    @if ( !empty($project['city']) and (!empty($project['year_start']) or !empty($project['month_start'])) ),&nbsp;@endif
                    {{ $project['month_start'] }} {{ $project['year_start'] }}
                    @if ( (!empty($project['month_start']) or !empty($project['year_start']))
                            and (!empty($project['month_end']) or !empty($project['year_end']) ) )&nbsp;-&nbsp;@endif
                    {{ $project['month_end'] }} {{ $project['year_end'] }}
                </p>
                <p><a href="#">{{ $project['link'] }}</a></p>
            </div>

            <p>{{ $project['description'] }}</p>
            <ul>
                @foreach($project['responsibilities_tags'] as $tag)
                <li>{{ $tag }}</li>
                @endforeach
            </ul>

        @endforeach

    @endif
@endif