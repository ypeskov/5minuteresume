<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    public function specialities()
    {
        return $this->belongsToMany('App\Speciality');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
