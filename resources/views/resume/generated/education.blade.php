@if ( $resume['educationContainInfo'] )

    @if ( sizeof($resume['education']) > 0 )

        <h2><?= mb_ucfirst(Lang::get('app.education')) ?></h2>


        <div class="position">
            @for($i=0; $i < sizeof($resume['education']); $i++)
                <p>{{ $resume['education'][$i]->university }}
                    @if ( !empty($resume['education'][$i]->university) and !empty($resume['education'][$i]->city) ),&nbsp;@endif
                    {{ $resume['education'][$i]->city }}
                    @if ( !empty($resume['education'][$i]->city) and !empty($resume['education'][$i]->year) ),&nbsp;@endif
                    {{ $resume['education'][$i]->year }}
                </p>
            @endfor
        </div>

    @endif

@endif