@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
        </ul>
    </div>

    <hr style="clear: left;" />

    <div>
        <table id="resume_table">
            <thead>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Title</th>
                <th>Display CV</th>
                <th>Delete CV</th>
            </thead>
            <tbody>
                @foreach($data['resumes'] as $item)
                    <tr>
                        <td>{{ $item['id'] }}</td>
                        <td>{{ $item['first_name'] }}</td>
                        <td>{{ $item['last_name'] }}</td>
                        <td>{{ $item['email'] }}</td>
                        <td><?= json_decode($item['speciality_tags'])[0]; ?></td>
                        <td><a href="/resume/html/{{ $item['id'] }}" target="_blank">HTML CV</a></td>
                        <td><a href="/admin/delete-resume/{{ $item['id'] }}">Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop