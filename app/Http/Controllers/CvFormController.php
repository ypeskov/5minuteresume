<?php
/**
 * User: Yuriy Peskov
 * Date: 2015-06-12
 * Time: 21:55
 */

namespace App\Http\Controllers;

use App\Skill;
use App\Speciality;
use App\Tag;
use App\PersonalQualityTag;
use \Response;
use \Request;
use \Config;

class CvFormController extends Controller
{
    public function showForm()
    {
        $data = [];

        $data['tags']           = Tag::with('specialities')->get()->toArray();
        $data['specialities']   = Speciality::with('tags')->with('skills')->get()->toArray();
        $data['skills']         = Skill::with('specialities')->get()->toArray();
        $data['qualities']      = PersonalQualityTag::all()->toArray();

        if ( Config::get('app.qtyVisibleTags') < sizeof($data['specialities']) ) {
            $data['visibleSpecialities'] = Config::get('app.qtyVisibleTags');
            $data['displayMoreSpecialities'] = true;
        } else {
            $data['visibleSpecialities'] = sizeof($data['specialities']);
            $data['displayMoreSpecialities'] = false;
        }

        if ( Config::get('app.qtyVisibleTags') < sizeof($data['qualities']) ) {
            $data['visibleQualities'] = Config::get('app.qtyVisibleTags');
            $data['displayMoreQualities'] = true;
        } else {
            $data['visibleQualities'] = sizeof($data['qualities']);
            $data['displayMoreQualities'] = false;
        }

        $view = view('resume.resume-form', ['data' => $data]);

        return $view;
    }
}
