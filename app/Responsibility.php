<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsibility extends Model
{
    public function specialities()
    {
        return $this->belongsToMany('App\Speciality');
    }
}
