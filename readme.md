Deployment instructions
=======================

 1. Run composer --install for installing all dependencies  
   
 2. Download and install [enter link description here](http://wkhtmltopdf.org/downloads.html). This tool makes conversion from HTML to PDF.  
   
 3. In the path: /public/css/all-pdf.css  find the extract with font:  
 @font-face {  
    font-family: 'museo_sans_cyrl500';  
    src: url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.eot');  
    src: url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.eot?#iefix') format('embedded-opentype'),  
    url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.woff2') format('woff2'),  
    url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.woff') format('woff'),  
    url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.ttf') format('truetype'),  
    url('http://resume.peskov.info/fonts/museosanscyrl_1-webfont.svg#museo_sans_cyrl500') format('svg');  
    font-weight: normal;  
    font-style: normal;  
}  
 And replace DOMAIN to the one which will be used for final product.  
 
 4. In the file /config/app.php  
 Change the strings   
     'images_domain' => 'http://resume.peskov.info',  
    'emailAddress'  => 'yuriy.peskov@gmail.com',  
to required values of production environment.  
   
 5.  Copy the file **.env.example**  to **.env**  
 Edit the next values:  
 APP_DEBUG=true -> true  
  
DB_HOST=localhost  
DB_DATABASE=it_cv  
DB_USERNAME=it_cv  
DB_PASSWORD=it_cv  
set here production environment DB settings.  
  
MAIL_DRIVER=smtp  
MAIL_HOST=mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=null  
MAIL_PASSWORD=null  
MAIL_ENCRYPTION=null  
Setup mail server settings here.  
   
 6. To initialize DB: run **php artisan migrate:refresh --seed** from command line from the root path of application.