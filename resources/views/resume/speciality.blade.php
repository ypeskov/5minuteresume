<div class="center-row">
    <div class="options-list"  id="speciality_wrapper">
        <div id="title_wrapper">
            <?= Form::text('personal_info[speciality]',
                    '',
                    ['id'   => 'resume_speciality',
                            'placeholder' => mb_ucfirst(Lang::get('app.speciality')),
                            'class' => '', ]);
            ?>
        </div>
    </div>

    <div class="tags_container choise-list">
        <div>
            <span class="visible_tags">
                @for($i=0; $i < $data['visibleSpecialities']; $i++)
                    <a href="" class="speciality_selector"
                       data-specialityId="{{ $data['specialities'][$i]['id'] }}"
                       data-specialityLabel="{{ $data['specialities'][$i]['speciality'] }}">
                        {{ $data['specialities'][$i]['speciality'] }}
                    </a>
                @endfor
            </span>

            @if ( $data['displayMoreSpecialities'] )
                <span>
                    <a href="#" class="more_tags"><?= Lang::get('app.more_tags') ?></a>
                </span>
            @endif
        </div>

        <div class="hidden more_tags_holder">
            <?php $specialitiesQty = sizeof($data['specialities']); ?>

            @for($i=$data['visibleSpecialities']; $i < $specialitiesQty; $i++)
                <a href="" class="speciality_selector"
                   data-specialityId="{{ $data['specialities'][$i]['id'] }}"
                   data-specialityLabel="{{ $data['specialities'][$i]['speciality'] }}">
                    {{ $data['specialities'][$i]['speciality'] }}
                </a>
            @endfor
        </div>
    </div>
</div>