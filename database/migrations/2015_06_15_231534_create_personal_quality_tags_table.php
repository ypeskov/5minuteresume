<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalQualityTagsTable extends Migration
{
    const MAX_PERSONAL_QUALITY_TAG_LENGTH = 100;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_quality_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quality_tag', self::MAX_PERSONAL_QUALITY_TAG_LENGTH);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_quality_tags');
    }
}
