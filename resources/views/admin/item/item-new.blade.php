@extends('layouts.admin')

@section('content')

    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
        </ul>
    </div>

    <?= Form::open(['name'  => 'skillsNewForm',
            'url'           => '/admin/add-item/' . $data['item'] ,
            'files'         => false,
            'id'            => '']);
    ?>

    <?= Form::text('itemLabel',
            '',
            ['class' => 'form-control', ]);
    ?>

    <div>
        <?= Form::submit(Lang::get('app.add'),
                ['id' => '',
                        'class' => 'btn btn-primary btn-lg'
                ]);
        ?>
    </div>

    <?= Form::close(); ?>
@stop