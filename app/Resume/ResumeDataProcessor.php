<?php

namespace App\Resume;

use App\Speciality;
use App\Resume;
use App\Tag;
use App\Skill;
use App\Responsibility;
use App\Project;
use App\ProfileTag;

class ResumeDataProcessor
{
    protected $resume;
    protected $tag;
    protected $speciality;

    public function __construct()
    {
        $this->resume   = new Resume();
        $this->tag      = new Tag();
        $this->speciality=new Speciality();
    }

    public function getSkillsForSpecialities($specialityId)
    {
        $specialitiesWithSkills = Speciality::with('skills')
            ->whereIn('id', $specialityId)
            ->get()
            ->toArray();

        $skills = [];
        foreach($specialitiesWithSkills as $speciality) {
            foreach($speciality['skills'] as $skill) {
                $skills[(int) $skill['id']] = [
                    'id'    => $skill['id'],
                    'skill' => $skill['skill'],
                ];
            }
        }

        return $skills;
    }

    public function getTagsFilteredBySkillAndSpeciality($skills, $specialityIds)
    {
        $tags = [];

        foreach($skills as $skillId => $skill) {
            $skillTags = Skill::distinct('t.tag_name')
                ->groupBy('t.tag_name')
                ->join('skill_tag as sk_t', 'sk_t.skill_id', '=', 'skills.id')
                ->join('tags as t', 'sk_t.tag_id', '=', 't.id')
                ->join('speciality_tag as sp_t', 'sp_t.tag_id', '=', 't.id')
                ->join('specialities as sp', 'sp_t.speciality_id', '=', 'sp.id')
                ->where('skills.id', '=', $skillId)
                ->whereIn('sp.id', $specialityIds)
                ->get()
                ->toArray();
            $tags[$skillId] = $skillTags;
        }

        return $tags;
    }

    public function getResponsibilityTagsForSpecialities($specialityIds)
    {
        return Responsibility::groupBy('responsibilities.responsibility_tag')
            ->join('responsibility_speciality as r_sp' ,
                'responsibilities.id', '=', 'r_sp.responsibility_id')
            ->join('specialities as sp', 'sp.id', '=', 'r_sp.speciality_id')
            ->whereIn('sp.id', $specialityIds)
            ->get()
            ->toArray();
    }

    public function getFilteredProfileTags($specialityIds)
    {
        return
            ProfileTag::groupBy('profile_tags.label')
                ->join('profile_tag_speciality as pt_sp',
                    'profile_tags.id', '=', 'pt_sp.profile_tag_id')
                ->join('specialities as sp', 'sp.id', '=', 'pt_sp.speciality_id')
                ->whereIn('sp.id', $specialityIds)
                ->get()
                ->toArray();
    }

    public function saveResume(Array $resumeData)
    {
        $this->setPersonalInfo($resumeData);

        \DB::transaction(function() use ($resumeData) {
            if ( $this->resume->save() ) {
                $this->saveProjects($resumeData['projects'], $this->resume->id);
            }
        });

        return $this->resume;
    }

    private function saveProjects(Array $projects, $resumeId)
    {
        $jsonedTags = function($tags) {
            return json_encode($tags);
        };

        $projectIds = [];
        Project::unguard();
        foreach($projects as $project) {
            if ( isset($project['responsibilities_tags']) ) {
                $project['responsibilities_tags'] =
                    $jsonedTags($project['responsibilities_tags']);
            } else {
                $project['responsibilities_tags'] = '';
            }

            $project['resume_id'] = $resumeId;
            $newProject = Project::create($project);
            if ( $newProject->id !== null ) {
                $projectIds[] = $newProject->id;
            }
        }
        Project::reguard();

        return $projectIds;
    }

    private function setPersonalInfo($resumeData)
    {
        $this->resume->first_name         = $resumeData['personalInfo']['firstName'];
        $this->resume->last_name          = $resumeData['personalInfo']['lastName'];
        $this->resume->city               = $resumeData['personalInfo']['city'];
        $this->resume->country            = $resumeData['personalInfo']['country'];
        $this->resume->email              = $resumeData['personalInfo']['email'];
        $this->resume->contact_details    = $resumeData['personalInfo']['contactDetails'];

        if ( isset($resumeData['resumeSpecialities']) ) {
            $this->resume->speciality_tags    =
                $this->prepareSpecialityTagsForSave($resumeData['resumeSpecialities']);
        } else {
            $this->resume->speciality_tags = '';
        }

        if ( isset($resumeData['skillTags']) ) {
            $this->resume->skill_tags         =
                $this->prepareSkillTagsForSave($resumeData['skillTags']);
        } else {
            $this->resume->skill_tags = '';
        }

        if ( isset($resumeData['quality_tags']) ) {
            $this->resume->quality_tags = json_encode($resumeData['quality_tags']);
        } else {
            $this->resume->quality_tags = '';
        }

        if ( isset($resumeData['profile_tags']) ) {
            $this->resume->profile_tags = json_encode($resumeData['profile_tags']);
        } else {
            $this->resume->profile_tags = '';
        }

        if ( isset($resumeData['educations']) ) {
            $this->resume->education = json_encode($resumeData['educations']);
        } else {
            $this->resume->education = '';
        }
    }

    /**
     * Prespare JSON array of specialities
     *
     * @param $specialities
     * @return JSON
     */
    private function prepareSpecialityTagsForSave($specialities)
    {
        $specialityTags = [];
        foreach($specialities as $speciality) {
            $specialityTags[] = $speciality['speciality'];
        }

        return json_encode($specialityTags);
    }

    private function prepareSkillTagsForSave($skillsWithTags)
    {
        $skillTags = [];
        foreach($skillsWithTags as $skillLabel => $skillWithTags) {
            foreach($skillWithTags as $tag) {
                $skillTags[$skillLabel][] = $tag['tagName'];
            }
        }

        return json_encode($skillTags);
    }
}