<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 2015-06-21
 * Time: 08:08
 */

namespace App\Resume;

use App\Resume;
use \Mail;
use \Lang;


class ResumeGenerator 
{
    protected $pdf;

    public function __construct(\mikehaertl\wkhtmlto\Pdf $pdf)
    {
        $this->pdf = $pdf;
    }

    private function projectContainInfo($resume)
    {
        $contain = false;

        foreach($resume['projects'] as $project) {
            if ( !empty($project['company_name'])
                or !empty($project['city'])
                or !empty($project['month_start'])
                or !empty($project['year_start'])
                or !empty($project['month_end'])
                or !empty($project['year_end'])
                or !empty($project['description'])
                or !empty($project['responsibilities_tags']) ) {

                $contain = true;
                break;
            }
        }

        return $contain;
    }

    private function educationContainInfo($resume)
    {
        $contains = false;

        $educations = json_decode($resume['education']);

        foreach($educations as $ed) {
            $ed = (Array) $ed;
            if ( !empty($ed['university'])
                or !empty($ed['city'])
                or !empty($ed['year']) ) {

                $contains = true;
                break;
            }
        }

        return $contains;
    }

    /**
     * Return resume processed for HTML output
     *
     * @param $id
     * @return mixed
     */
    public function getPreparedResume($id)
    {
        $resume = Resume::with('projects')->find($id)->toArray();

        $resume['projectContainInfo'] = $this->projectContainInfo($resume);
        $resume['educationContainInfo'] = $this->educationContainInfo($resume);

        $resume['profile_tags'] = json_decode($resume['profile_tags']);

        if ( ($resume['skill_tags'] = json_decode($resume['skill_tags'])) === null ) {
            $resume['skill_tags'] = [];
        }

        $resume['quality_tags'] = json_decode($resume['quality_tags']);
        $resume['speciality_tags'] = json_decode($resume['speciality_tags']);
        $resume['education'] = json_decode($resume['education']);

        foreach($resume['projects'] as &$project) {
            $responsibilitiesDecoded = json_decode($project['responsibilities_tags']);

            if ( $responsibilitiesDecoded !== null ) {
                $project['responsibilities_tags'] = json_decode($project['responsibilities_tags']);
            } else {
                $project['responsibilities_tags'] = [];
            }

        }

        return $resume;
    }

    protected function preparePdfOptions($resume)
    {
        $footerHTML = '<div style="text-align: right;height: 35mm; vertical-align: middle; ">';
        $footerHTML .= '<span style="font-size: 10px;">' . mb_ucfirst(Lang::get('app.created_with')) . '</span>&nbsp';
        $footerHTML .= '<span style="color: red;font-size: 10px;">' . Lang::get('app.5min') . '</span>';
        $footerHTML .= '<span style="font-size: 10px;">' . Lang::get('app.resume_com') . '</span>';
        $footerHTML .= '</div>';

        $this->pdf->setOptions([
            'header-html'       => '<div style="height: 15mm;"></div>',
//            'header-line',
            'header-spacing'    => 5,
//            'footer-right'  => 'Created with 5minresume.com',
            'footer-html'       => $footerHTML,
            'footer-line',
            'footer-spacing'    => 5,
        ]);
    }

    public function getPdfResume($id)
    {
        $resume = $this->getPreparedResume($id);

        $html = \View::make('resume.generated-cv',
            ['resume' => $resume,
                'isHtml'    => false,
            ]
        )->render();

        $this->preparePdfOptions($resume);

        return $this->pdf->addPage($html)->send();
    }

    private function getMailAttachmentName($pdfName, $resume)
    {
        $newName = '/tmp/' . $resume['first_name'] . '_' . $resume['last_name'] . ' cv.pdf';

        if ( copy($pdfName, $newName) ) {
            return [true, $newName];
        } else {
            return [false, $newName];
        }
    }

    public function sendPdfResume($id)
    {

        $resume = $this->getPreparedResume($id);

        $html = \View::make('resume.generated-cv',
            ['resume' => $resume,
                'isHtml'    => false,
            ]
        )->render();

        $this->preparePdfOptions($resume);

        $fileName = 'pdf/resume_' . (int) $id . '.pdf';

        if ( $this->pdf->addPage($html)->saveAs($fileName) ) {

            list($isSent, $attachName) = $this->getMailAttachmentName($fileName, $resume);

            if ( $isSent ) {
                Mail::send('resume.email', ['resume' => $resume],
                    function ($email) use ($resume, $id, $attachName)  {
                        $email
                            ->to($resume['email'])
                            ->subject('Resume: ' . $resume['first_name'] . ' '  . $resume['last_name'])
                            ->from(\Config::get('app.emailAddress'))
                            ->attach($attachName);
                });

                return true;
            } else {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}