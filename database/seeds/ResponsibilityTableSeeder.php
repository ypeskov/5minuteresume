<?php

use Illuminate\Database\Seeder;
use App\Responsibility;

class ResponsibilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Responsibility::create([
            'id'    => 1,
            'responsibility_tag' => 'Regression testing',
        ]);

        Responsibility::create([
            'id'    => 2,
            'responsibility_tag' => 'Post release deployment acceptance testing',
        ]);

        Responsibility::create([
            'id'    => 3,
            'responsibility_tag' => 'Creating test cases',
        ]);

        Responsibility::create([
            'id'    => 4,
            'responsibility_tag' => 'Develop application\'s architecture',
        ]);
    }
}
