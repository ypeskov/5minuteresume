<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>5 Minutes Resume</title>

    <link rel="stylesheet" href="/vendor/tag-it/css/jquery.tagit.css" type="text/css">

    <script src="js/main.js" type="text/javascript"></script>
    <script type="text/javascript">
        // replace custom forms
        jcf.lib.domReady(function(){
            jcf.customForms.replaceAll();
        });
    </script>

    <link rel="stylesheet" href="/css/all.css" type="text/css">
    <link rel="stylesheet" href="/css/extra_style.css" type="text/css">

</head>

<body>
<div id="wrapper">

    @yield('content')

    @if ( isset($data) )
        <div id="special_data">
            <script type="text/javascript">
                var allSpecialities = <?= json_encode($data['specialities']) ?>;
                var allTags = <?= json_encode($data['tags']) ?>;
                var allSkills = <?= json_encode($data['skills']) ?>;
            </script>
        </div>
    @endif

</div>

<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/tag-it/js/tag-it.min.js"></script>

<script type="text/javascript">
    var qtyVisibleTags = <?= Config::get('app.qtyVisibleTags') ?>;
    var qtyTagsToAdd = <?= Config::get('app.qtyTagsToAdd') ?>;
</script>

<script src="/js/resume.js"></script>



</body>

</html>