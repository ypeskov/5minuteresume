<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    const MAX_FIRST_NAME_LENGTH = 30;
    const MAX_LAST_NAME_LENGTH  = 50;
    const MAX_CITY_LENGTH       = 50;
    const MAX_COUNTRY_LENGTH    = 50;
    const MAX_EMAIL_LENGTH      = 100;
    const MAX_SPECIALITY_TAGS_LENGTH = 255;
    const MAX_SKILL_TAGS_LENGTH = 1024;
    const MAX_QUALITY_TAGS_LENGTH   = 512;
    const MAX_PROFILE_TAG_LENGTH    = 512;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name', self::MAX_FIRST_NAME_LENGTH);
            $table->string('last_name', self::MAX_LAST_NAME_LENGTH);
            $table->string('city', self::MAX_CITY_LENGTH);
            $table->string('country', self::MAX_COUNTRY_LENGTH);
            $table->string('email', self::MAX_EMAIL_LENGTH);
            $table->text('contact_details');
            $table->string('speciality_tags', self::MAX_SPECIALITY_TAGS_LENGTH);
            $table->string('quality_tags', self::MAX_QUALITY_TAGS_LENGTH);
            $table->string('skill_tags', self::MAX_SKILL_TAGS_LENGTH);
            $table->string('profile_tags', self::MAX_PROFILE_TAG_LENGTH);
            $table->string('education', 1000);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resumes');
    }
}
