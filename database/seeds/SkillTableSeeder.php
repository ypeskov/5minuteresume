<?php

use Illuminate\Database\Seeder;
use App\Skill;

class SkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::create([
            'id'    => 1,
            'skill' => 'Programming languages'
        ]);

        Skill::create([
            'id'    => 2,
            'skill' => 'API'
        ]);

        Skill::create([
            'id'    => 3,
            'skill' => 'Methodologies'
        ]);

        Skill::create([
            'id'    => 4,
            'skill' => 'OS'
        ]);

        Skill::create([
            'id'    => 5,
            'skill' => 'Databases'
        ]);

        Skill::create([
            'id'    => 6,
            'skill' => 'Frameworks'
        ]);

        Skill::create([
            'id'    => 7,
            'skill' => 'Platforms'
        ]);

        Skill::create([
            'id'    => 8,
            'skill' => 'Tools'
        ]);
    }
}
