<h2 class="profile"><?= mb_ucfirst(Lang::get('app.profile')) ?></h2>

<div class="options-list">
    <ul id="profile_tags"></ul>
</div>


<div id="available_profile_tags">
    <span class="visible_tags"></span>

    <span>
        <a href="#" class="more_tags hidden"><?= Lang::get('app.more_tags') ?></a>
    </span>
</div>

<div class="hidden more_tags_holder dynamic_tags_holder"></div>

<span id="profile_tag_template" class="hidden rowly_tag_item"><a href=""></a></span>
