<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTagSpecialityPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_tag_speciality', function(Blueprint $table) {
            $table->integer('profile_tag_id')->unsigned()->index();
            $table->foreign('profile_tag_id')->references('id')->on('profile_tags')->onDelete('cascade');
            $table->integer('speciality_id')->unsigned()->index();
            $table->foreign('speciality_id')->references('id')->on('specialities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_tag_speciality');
    }
}
