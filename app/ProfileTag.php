<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileTag extends Model
{
    public function specialities()
    {
        return $this->belongsToMany('App\Speciality');
    }
}
