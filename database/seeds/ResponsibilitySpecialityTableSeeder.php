<?php

use Illuminate\Database\Seeder;
use App\Speciality;

class ResponsibilitySpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Speciality::find(3)->responsibilities()->sync([1, 2, 3, ]);
        Speciality::find(1)->responsibilities()->sync([4, ]);
        Speciality::find(2)->responsibilities()->sync([4, ]);
    }
}
