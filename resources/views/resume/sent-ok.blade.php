@extends('layouts.main')

@section('content')
    <p>Congratulations!</p>
    <p>Your resume has been sent to <a href="mailto:{{ $resume['email'] }}">{{ $resume['email'] }}</a>. Please check your inbox.</p>
@stop