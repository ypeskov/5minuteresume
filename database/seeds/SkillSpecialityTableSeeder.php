<?php

use Illuminate\Database\Seeder;

use App\Speciality;

class SkillSpecialityTableSeeder extends Seeder
{
    public function run()
    {
        Speciality::find(1)->skills()->sync([1, 2, 3, 4, 5, 6, 7, 8, ]);
        Speciality::find(3)->skills()->sync([3, 4, 5, 8, ]);
        Speciality::find(2)->skills()->sync([1, 2, 3, 3, 4, 5, 6, 7, 8, ]);
        Speciality::find(5)->skills()->sync([1, 3, 8, ]);
    }
}
