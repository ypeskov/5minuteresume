@extends('layouts.main')

@section('content')

    <?= Form::open(['name'  => 'resumeForm',
            'url'           => '/create-resume',
            'files'         => false,
            'id'            => 'resumeDataForm']);
    ?>

    <section>
        @include('resume.personal-info')

        @include('resume.speciality')
    </section>

    <section>
        @include('resume.profile')
    </section>

    <section>
        @include('resume.skills')
    </section>

    <section>
        @include(('resume.experience'))
    </section>

    <section>
        @include('resume.education')
    </section>

    <section>
        @include('resume.personal_quality_tags')
    </section>

    <div class="btn-row">
        <div class="holder">
            <?= Form::submit(mb_ucfirst(Lang::get('app.save_to_pdf')),
                    ['id' => 'submit_resume',
                            'class' => ''
                    ]);
            ?>
        </div>
        <p>Resume file will be sent to your email.</p>
    </div>

    <?= Form::close(); ?>

@stop
