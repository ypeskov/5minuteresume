<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function resumes()
    {
        return $this->belongsTo('App\Resume');
    }
}
