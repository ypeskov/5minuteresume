<?php

use Illuminate\Database\Seeder;
use \App\Speciality;

class SpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Speciality::create([
            'id'            => 1,
            'speciality'    => 'Web Developer',
        ]);

        Speciality::create([
            'id'            => 2,
            'speciality'    => 'Java Developer',
        ]);

        Speciality::create([
            'id'            => 3,
            'speciality'    => 'QA Engineer',
        ]);

        Speciality::create([
            'id'            => 4,
            'speciality'    => 'JS Developer',
        ]);

        Speciality::create([
            'id'            => 5,
            'speciality'    => 'Project Manager',
        ]);
    }
}
