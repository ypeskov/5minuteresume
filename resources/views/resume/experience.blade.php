<h2 class="experience"><?= mb_ucfirst(Lang::get('app.professional_experience')) ?></h2>

<div class="experience_item">

    <form action="#">
        <fieldset>
            <div class="row">
                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('experience[company_name]',
                                '',
                                ['placeholder' => mb_ucfirst(Lang::get('app.company_name')),
                                        'class' => 'form-control project_company_name',
                                ]);
                        ?>
                    </div>
                </div>
                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('experience[city]',
                                '',
                                ['placeholder' => mb_ucfirst(Lang::get('app.city')),
                                        'class' => 'form-control project_city',
                                ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row verticav-fields">
                <div class="col">
                    <div class="col-holder">
                        <div class="col">
                            <div class="input-holder">
                                <?= Form::text('experience[month_start]',
                                        '',
                                        ['placeholder' => mb_ucfirst(Lang::get('app.month')),
                                                'class' => 'form-control project_month_start',
                                        ]);
                                ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-holder">
                                <?= Form::text('experience[year_start]',
                                        '',
                                        ['placeholder' => mb_ucfirst(Lang::get('app.year')),
                                                'class' => 'form-control project_year_start',
                                        ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="decor">-</div>

                <div class="col">
                    <div class="col-holder">
                        <div class="col">
                            <div class="input-holder">
                                <?= Form::text('experience[month_end]',
                                        '',
                                        ['placeholder' => mb_ucfirst(Lang::get('app.month')),
                                                'class' => 'form-control project_month_end',
                                        ]);
                                ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-holder">
                                <?= Form::text('experience[year_end]',
                                        '',
                                        ['placeholder' => mb_ucfirst(Lang::get('app.year')),
                                                'class' => 'form-control project_year_end',
                                        ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col large">
                    <div class="input-holder">
                        <?= Form::text('experience[link]', '', [
                                'placeholder' => mb_ucfirst(Lang::get('app.link')),
                                'class' => 'form-control project_link',
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="row ta-row">
                <div class="col large">
                    <div class="input-holder">
                        <?= Form::textarea('experience[description]', '', [
                                'placeholder'   => mb_ucfirst(Lang::get('app.project_description')),
                                'class' => 'form-control project_description',
                        ]) ?>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <h3>Responsibilities:</h3>

    <div class="options-list">
        <ul class="responsibilities_tags"></ul>
    </div>

    <div class="available_responsibilities_tags">
        <span class="visible_tags"></span>

        <span>
            <a href="#" class="more_tags hidden"><?= Lang::get('app.more_tags') ?></a>
        </span>
    </div>

    <div class="hidden more_tags_holder dynamic_tags_holder"></div>

</div>

<div>
    <span>
        <a href="#" class="add-project-link add_project"><?= mb_ucfirst(Lang::get('app.add_project')) ?></a>
    </span>
</div>