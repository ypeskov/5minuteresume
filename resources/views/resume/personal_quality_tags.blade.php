<h2 class="qualities"><?= mb_ucfirst(Lang::get('app.personal_qualities')) ?></h2>


<div class="options-list">
    <ul id="personal_qualities"></ul>
</div>

<div class="qualities_box">
    <div id="qualities_container">
        <span class="visible_tags">
            @for($i=0; $i < $data['visibleQualities']; $i++)
                <a href="#">{{ $data['qualities'][$i]['quality_tag'] }}</a>
                @if ( $i < ($data['visibleQualities'] - 1) )<span> </span>@endif
            @endfor
        </span>

        @if ( $data['displayMoreQualities'] )
            <span>
                <a href="#" class="more_tags"><?= Lang::get('app.more_tags') ?></a>
            </span>
        @endif
    </div>

    <div class="hidden more_tags_holder">
        @for($i=$data['visibleQualities']; $i < sizeof($data['qualities']); $i++)
            <a href="#">{{ $data['qualities'][$i]['quality_tag'] }}</a>
        @endfor
    </div>
</div>
