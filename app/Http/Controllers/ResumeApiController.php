<?php

namespace App\Http\Controllers;

use App\Resume;
use \Response;
use \Request;
use App\Resume\ResumeDataProcessor;
use App\Resume\ResumeGenerator;
use \mikehaertl\wkhtmlto\Pdf;

class ResumeApiController extends Controller
{
    public function getFilteredSkillsAction()
    {
        $resumeProcessor = new ResumeDataProcessor();

        $specialityIds = explode(',', Request::get('specialityId'));

        $skills = $resumeProcessor->getSkillsForSpecialities($specialityIds);

        $tags = $resumeProcessor->getTagsFilteredBySkillAndSpeciality($skills, $specialityIds);

        $responsibilityTags =
            $resumeProcessor->getResponsibilityTagsForSpecialities($specialityIds);

        $availableProfileTags = $resumeProcessor->getFilteredProfileTags($specialityIds);

        $result = [
            'skills'    => $skills,
            'tags'      => $tags,
            'availableResponsibilityTags' => $responsibilityTags,
            'availableProfileTags'  => $availableProfileTags,
        ];

        return Response::json($result);
    }

    /**
     * This action is used for storing to DB the new resume.
     *
     * @return JSON
     */
    public function newResumeAction()
    {
        $response = [
            'error'     => false,
            'message'   => '',
            'errorCode' => '0',
        ];

        $resumeData = Request::all()['resumeData'];

        $resumeProcessor = new ResumeDataProcessor();

        $resume = $resumeProcessor->saveResume($resumeData);

        if ( $resume->id === null ) {
            $response = [
                'error'     => true,
                'message'   => 'Cannot save resume',
                'errorCode' => '10',
            ];
        } else {
            $response['data']['resume'] = $resume->toArray();
        }

        return Response::json($response);
    }

    public function sendResumeAction($id)
    {
        $resumeId = (int) $id;

        $pdf = new Pdf();
        $resumeGenerator = new ResumeGenerator($pdf);

        $resume = $resumeGenerator->getPreparedResume($id);

        $ret = [
            'success'   => true,
            'errorCode' => 0,
        ];

        $isSent = false;
        try {
            $resumeGenerator->sendPdfResume($id);

            $isSent = true;
        } catch(\Exception $e) {
            $isSent = false;
        }

        if ( $isSent ) {
            return Response::json($ret);
        } else {
            $ret['success']     = false;
            $ret['errorCode']   = 10;

            return Response::json($ret);
        }
    }
}