<h1 class="logo"><a href="#">Resume 5min</a></h1>

<div class="form-holder">
    <form action="#">
        <fieldset>
            <div class="row">
                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('personal_info[first_name]',
                                '',
                                ['id'   => 'first_name',
                                        'placeholder' => mb_ucfirst(Lang::get('app.first_name')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                </div>

                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('personal_info[last_name]',
                                '',
                                ['id'   => 'last_name',
                                        'placeholder' => mb_ucfirst(Lang::get('app.last_name')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('personal_info[city]',
                                '',
                                ['id'   => 'city',
                                        'placeholder' => mb_ucfirst(Lang::get('app.city')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                </div>

                <div class="col">
                    <div class="input-holder">
                        <?= Form::text('personal_info[country]',
                                '',
                                ['id'   => 'country',
                                        'placeholder' => mb_ucfirst(Lang::get('app.country')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col large">
                    <div class="input-holder">
                        <?= Form::text('personal_info[email]',
                                '',
                                ['id'   => 'email',
                                        'placeholder' => mb_ucfirst(Lang::get('app.email')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                    <span class="text hidden" id="email_error"><?= mb_ucfirst(Lang::get('app.email_field_required')); ?></span>
                </div>
            </div>

            <div class="row">
                <div class="col large">
                    <div class="input-holder">
                        <?= Form::text('personal_info[contact_details]',
                                '',
                                ['id'   => 'contact_details',
                                        'placeholder' => mb_ucfirst(Lang::get('app.contact_details')),
                                        'class' => '',
                                ]);
                        ?>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>