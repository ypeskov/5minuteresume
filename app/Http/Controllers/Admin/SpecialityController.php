<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\Speciality;


class SpecialityController extends BaseItemController
{
    public function indexAction()
    {
        $data['specialities'] = Speciality::all()->toArray();

        return view('admin.speciality.speciality-index', ['data' => $data]);
    }
}