<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\Responsibility;


class ResponsibilityController extends BaseItemController
{
    public function indexAction()
    {
        $data['tags'] = Responsibility::all()->toArray();

        return view('admin.responsibility.responsibility-index', ['data' => $data]);
    }
}