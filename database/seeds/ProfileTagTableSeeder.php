<?php

use Illuminate\Database\Seeder;
use App\ProfileTag;

class ProfileTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfileTag::create([
            'id'    => 1,
            'label' => 'Object Oriented Development',
        ]);

        ProfileTag::create([
            'id'    => 2,
            'label' => 'Server Development',
        ]);

        ProfileTag::create([
            'id'    => 3,
            'label' => 'Web Application Development',
        ]);

        ProfileTag::create([
            'id'    => 4,
            'label' => 'Qaulity Assuarence process building',
        ]);

        ProfileTag::create([
            'id'    => 5,
            'label' => 'Agile Development',
        ]);
    }
}
