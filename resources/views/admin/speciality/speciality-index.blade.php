@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/speciality">Edit speciality</a></li>
            <li><a href="/admin/new-item/speciality">New Speciality</a></li>
        </ul>

    </div>

    <hr style="clear: left;" />

    <div>
        <table class="items_container">
        @foreach($data['specialities'] as $speciality)
            <tr>
                <td>{{ $speciality['id'] }}</td>
                <td>{{ $speciality['speciality'] }}</td>
                <td>
                    <a href="/admin/item-delete/speciality/{{ $speciality['id'] }}">
                        {{ Lang::get('app.delete') }}
                    </a>
                </td>
            </tr>
        @endforeach
        </table>
    </div>
@stop