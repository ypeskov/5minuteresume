<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');
        $this->call('TagTableSeeder');
        $this->call('SpecialityTableSeeder');

        $this->call('SpecialityTagTableSeeder');

        $this->call('SkillTableSeeder');

        $this->call('SkillSpecialityTableSeeder');
        $this->call('SkillTagTableSeeder');

        $this->call('ResponsibilityTableSeeder');
        $this->call('ResponsibilitySpecialityTableSeeder');

        $this->call('PersonalQualityTagTableSeeder');

        $this->call('ProfileTagTableSeeder');
        $this->call('ProfileTagSpecialityTableSeeder');

        Model::reguard();
    }
}


