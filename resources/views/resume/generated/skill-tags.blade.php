@if ( sizeof($resume['skill_tags']) > 0 )

    <h2><?= mb_ucfirst(Lang::get('app.technical_skills')) ?></h2>

    @foreach($resume['skill_tags'] as $skillLabel => $skillTags)
        <dt>{{ $skillLabel }}</dt>
        <dd><?= htmlJoin(', ', 'span', $skillTags) ?></dd>
    @endforeach

@endif
