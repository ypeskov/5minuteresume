@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/personal-quality">Edit personal quality tags</a></li>
            <li><a href="/admin/new-item/personal-quality">New Personal Quality Tag</a></li>
        </ul>
    </div>

    <div>
        <table class="items_container">
            @foreach($data['tags'] as $tag)
                <tr>
                    <td>{{ $tag['id'] }}</td>
                    <td>{{ $tag['quality_tag'] }}</td>
                    <td>
                        <a href="/admin/item-delete/tag/{{ $tag['id'] }}">
                            {{ Lang::get('app.delete') }}
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@stop