@extends('layouts.main')

@section('content')
    <p>We are sorry.</p>
    <p>Some error occurred while sending your resume. Please try again later.</p>
@stop