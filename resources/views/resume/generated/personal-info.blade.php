<h2><?= mb_ucfirst(Lang::get('app.personal_information')); ?></h2>

@if ( !empty($resume['first_name']) or !empty($resume['last_name']) )
    <div class="row">
        <strong><?= mb_ucfirst(Lang::get('app.name')); ?>:</strong>
        {{ $resume['first_name'] }}@if ( !empty($resume['first_name']) and !empty($resume['last_name']) ),&nbsp;@endif {{ $resume['last_name'] }}
    </div>
@endif

@if ( !empty($resume['city']) or !empty($resume['country']) )
    <div class="row">
        <strong><?= mb_ucfirst(Lang::get('app.address')); ?>:</strong>
        {{ $resume['city'] }}@if ( !empty($resume['city']) and !empty($resume['country']) ),&nbsp;@endif{{ $resume['country'] }}
    </div>
@endif

@if ( !empty($resume['email']) )
    <div class="row">
        <strong><?= mb_ucfirst(Lang::get('app.email')); ?>:</strong>
        {{ $resume['email'] }}
    </div>
@endif

@if ( !empty($resume['contact_details']) )
    <div class="row">
        <strong><?= mb_ucfirst(Lang::get('app.contact_details')); ?>:</strong>
        {{ $resume['contact_details'] }}
    </div>
@endif