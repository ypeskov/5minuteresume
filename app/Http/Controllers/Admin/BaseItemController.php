<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 2015-06-18
 * Time: 01:13
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use \Request;
use \Redirect;

class BaseItemController extends Controller
{
    protected $prefix;

    protected $data = [];

    public  $items = [
        'skill' => [
            'labelField'    => 'skill',
            'modelClass'    => 'App\Skill',
            'relations' => [
                'speciality'    => 'specialities',
            ]
        ],
        'speciality'    => [
            'labelField'    => 'speciality',
            'modelClass'    => 'App\Speciality',
            'relations' => [
                'skill'     => 'skills',
            ]
        ],
        'profile-tag'   => [
            'labelField'    => 'label',
            'modelClass'    => 'App\ProfileTag',
            'relations' => [
                'speciality'    => 'specialities',
            ]
        ],
        'tag'   => [
            'labelField'    => 'tag_name',
            'modelClass'    => 'App\Tag',
            'relations' => [
                'speciality'    => 'specialities',
                'skill'         => 'skills',
            ]
        ],
        'personal-quality'   => [
            'labelField'    => 'quality_tag',
            'modelClass'    => 'App\PersonalQualityTag',
            'relations' => [],
        ],
        'responsibility'   => [
            'labelField'    => 'responsibility_tag',
            'modelClass'    => 'App\Responsibility',
            'relations' => [
                'speciality'    => 'specialities',
            ],
        ]
    ];

    public function newItemAction($item)
    {
        $viewName = 'admin.item.item-new';

        $this->data['item']  = $item;

        return view($viewName, ['data' => $this->data,]);
    }

    public function addItemAction($item)
    {
        $itemArr = $this->items[$item];

        $newItem = new $itemArr['modelClass']();

        $newItem->$itemArr['labelField'] = Request::get('itemLabel', 'default skill');

        if ($newItem->save()) {
            return Redirect::route('admin' . ucfirst($item))->with('flashMessage',
                \Lang::get('app.added') . ':&nbsp;' . $newItem->$itemArr['labelField']);
        } else {
            return Redirect::route('admin' . ucfirst($item))->with('flashWarning',
                \Lang::get('app.not_added') . ':&nbsp;' . $newItem->$itemArr['labelField']);
        }
    }

    public function editItemAction($item)
    {
        $itemClassName = $this->items[$item]['modelClass'];
        $itemModel = new $itemClassName();

        $data['items']  = $itemModel->all();
        $data['item']   = $item;
        $data['fieldLabel'] = $this->items[$item]['labelField'];

        return view('admin.item.item-edit', ['data' => $data,]);
    }

    public function updateItemAction($item)
    {
        $itemModelClass = $this->items[$item]['modelClass'];


        $itemsToUpdate = Request::all()['items'];

        $updatedAll = true;
        foreach($itemsToUpdate as $itemId => $itemLabel) {
            $itemObj = new $itemModelClass();

            $itemToUpdate = $itemObj->find((int) $itemId);
            $labelField = $this->items[$item]['labelField'];

            if ( strlen($itemLabel) > 0 ) {
                $itemToUpdate->$labelField = $itemLabel;
                if ( !$itemToUpdate->save() ) {
                    $updatedAll = false;
                }
            } else {
                continue;
            }
        }

        if ( $updatedAll === true) {
            $status = \Lang::get('app.updated');
        } else {
            $status = \Lang::get('app.not_updating');
        }

        return Redirect::route('admin' . ucfirst($item))->with('flashMessage', $status);
    }

    public function deleteItemAction($item, $itemId)
    {
        $itemArr = $this->items[$item];
        $newObj = new $itemArr['modelClass']();

        if ( $newObj::destroy((int) $itemId) > 0 ) {
            return Redirect::route('admin' . ucfirst($item))->with('flashMessage',
                \Lang::get('app.deleted'));
        } else {
            return Redirect::route('admin' . ucfirst($item))->with('flashMessage',
                \Lang::get('app.not_deleted'));
        }
    }

    public function assocItemEditAction($itemHolder, $itemRelated)
    {
        $className = $this->items[$itemHolder]['modelClass'];

        $itemHolderObj = new $className();

        $this->data['itemHolderWithRelated'] =
            $itemHolderObj
                ->with($this->items[$itemHolder]['relations'][$itemRelated])
                ->get()
                ->toArray();

        $className = $this->items[$itemRelated]['modelClass'];
        $this->data['relatedItems'] = (new $className())->all();

        $this->data['qtyRelatedItems'] = sizeof($this->data['relatedItems']);
        $this->data['relatedItemLabelField'] = $this->items[$itemRelated]['labelField'];
        $this->data['holderItemLabelField'] = $this->items[$itemHolder]['labelField'];
        $this->data['holderItem']   = $itemHolder;
        $this->data['relatedItem']   = $itemRelated;

        return view('admin.item.item-assoc-edit', [
            'data' => $this->data,
            'isItemRelated' => [__CLASS__, 'isItemRelated'],
        ]);
    }

    static public function isItemRelated($internalItemId, $holderItem,
                                         $holderItemLabel, $relatedItemLabel)
    {
        $ret = false;

        $myself = new self();

//        $holderFieldLabel= $myself->items[$holderItemLabel]['labelField'];
        $relatedFields= $myself->items[$holderItemLabel]['relations'][$relatedItemLabel];

        $holderAndSpecialities = $holderItem[$relatedFields];

        foreach ($holderAndSpecialities as $relatedItems) {
            if ((int)$relatedItems['id'] === (int) $internalItemId) {
                $ret = true;
                break;
            }
        }

        return $ret;
    }

    public function assocItemUpdateAction($holderItem, $relatedItem)
    {
        $holderItemsToChange = Request::all()['holderItems'];
        $holderClassName = $this->items[$holderItem]['modelClass'];

        $relatedItemsProperty = $this->items[$holderItem]['relations'][$relatedItem];

        foreach((new $holderClassName())->all()->toArray() as $holderItemsArr) {
            $holderItemObj = new $holderClassName();
            $holderItemId = (int) $holderItemsArr['id'];

            if ( array_key_exists($holderItemId, $holderItemsToChange) ) {
                $holderItemObj
                    ->find($holderItemId)
                    ->$relatedItemsProperty()
                    ->sync(array_keys($holderItemsToChange[(int) $holderItemId]));
            } else {
                $holderItemObj
                    ->find($holderItemId)
                    ->$relatedItemsProperty()
                    ->sync([]);
            }
        }

        return Redirect::route('admin' . ucfirst($holderItem))->with('flashMessage',
            \Lang::get('app.updated'));
    }
}