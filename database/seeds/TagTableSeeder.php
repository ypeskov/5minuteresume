<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'id'        => 1,
            'tag_name'  => 'php'
        ]);

        Tag::create([
            'id'        => 2,
            'tag_name'  => 'laravel'
        ]);

        Tag::create([
            'id'        => 3,
            'tag_name'  => 'SCRUM'
        ]);

        Tag::create([
            'id'        => 4,
            'tag_name'  => 'Linux',
        ]);

        Tag::create([
            'id'        => 5,
            'tag_name'  => 'Windows',
        ]);

        Tag::create([
            'id'        => 6,
            'tag_name'  => 'iOS',
        ]);

        Tag::create([
            'id'        => 7,
            'tag_name'  => 'Java',
        ]);

        Tag::create([
            'id'        => 8,
            'tag_name'  => 'JBOSS',
        ]);

        Tag::create([
            'id'        => 9,
            'tag_name'  => 'MySQL',
        ]);

        Tag::create([
            'id'        => 10,
            'tag_name'  => 'Oracle',
        ]);

        Tag::create([
            'id'        => 11,
            'tag_name'  => 'Postgres',
        ]);
    }
}
