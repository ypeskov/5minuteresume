@extends('layouts.admin')

@section('content')
    <div>
        <ul class="admin_menu_container">
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/edit-item/tag">Edit tags</a></li>
            <li><a href="/admin/edit-item-assoc/tag/speciality">Tag2Speciality</a></li>
            <li><a href="/admin/edit-item-assoc/tag/skill">Tag2Skill</a></li>
            <li><a href="/admin/new-item/tag">New Tag</a></li>
        </ul>
    </div>

    <div>
        <table class="items_container">
            @foreach($data['tags'] as $tag)
                <tr>
                    <td>{{ $tag['id'] }}</td>
                    <td>{{ $tag['tag_name'] }}</td>
                    <td>
                        <a href="/admin/item-delete/tag/{{ $tag['id'] }}">
                            {{ Lang::get('app.delete') }}
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@stop