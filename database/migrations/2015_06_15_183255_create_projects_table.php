<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    const MAX_COMPANY_NAME_LENGTH   = 100;
    const MAX_CITY_LENGTH           = 50;
    const MAX_LINK_LENGTH           = 100;
    const MAX_TAGS_LENGTH           = 512;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', self::MAX_COMPANY_NAME_LENGTH);
            $table->string('city', self::MAX_CITY_LENGTH);
            $table->string('month_start', 20);
            $table->string('year_start', 20);
            $table->string('month_end', 20);
            $table->string('year_end', 20);
            $table->string('link', self::MAX_LINK_LENGTH);
            $table->text('description');
            $table->string('responsibilities_tags', self::MAX_TAGS_LENGTH);

            $table->integer('resume_id')->unsigned();
            $table->foreign('resume_id')
                ->references('id')
                ->on('resumes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
