
<div class="header-holder">
    <h2>{{ $resume['first_name'] }}&nbsp;{{ $resume['last_name'] }}</h2>
    <h3>
        @if (sizeof($resume['speciality_tags']) > 0)
            @foreach($resume['speciality_tags'] as $tag)
                <p class="resume_title">{{ $tag }}</p>
            @endforeach
        @endif
    </h3>
</div>

<h1 class="logo"><a href="#"><img src="<?= Config::get('app.images_domain') ?>/images/logo-pdf.png" width="122" height="39" alt="image description"/></a></h1>