<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\ProfileTag;


class ProfileTagController extends BaseItemController
{
    public function indexAction()
    {
        $data['tags'] = ProfileTag::all()->toArray();

        return view('admin.profile-tag.profile-tag-index', ['data' => $data]);
    }
}