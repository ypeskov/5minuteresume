<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\Skill;


class SkillController extends BaseItemController
{
    public function indexAction()
    {
        $data['skills'] = Skill::all()->toArray();

        return view('admin.skills.skills-index', ['data' => $data]);
    }
}