<?php
/**
 * User: Yuriy Peskov
 * Date: 2015-06-17
 * Time: 07:11
 */

namespace App\Http\Controllers\Admin;

use \Redirect;
use App\Http\Controllers\Controller;
use App\Resume;

class AdminController extends Controller
{
    const ADMIN_PREFIX = 'admin';

    protected $prefix;

    public function __construct()
    {
        if ( !isset($_SERVER['PHP_AUTH_USER']) ) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'No access';
            exit;
        }

        $this->prefix = self::ADMIN_PREFIX;
    }

    public function dashboardAction()
    {
        $data = [];
        $data['adminPrefix'] = $this->prefix;

        return view('admin.dashboard', ['data' => $data]);
    }

    public function resumeListAction()
    {
        $data = [];

        $data['resumes'] = Resume::all()->toArray();

        return view('admin.list-resumes', ['data' => $data, ]);
    }

    public function deleteResumeAction($id)
    {
        $resume = Resume::find((int) $id);
        if ( $resume !== null ) {
            $resume->delete();
        }

        return Redirect::route('listResumesAction')->with('flashMessage',
            \Lang::get('app.resume') . ' ' . $id . ': '  . \Lang::get('app.deleted'));
    }
}