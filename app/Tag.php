<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function specialities()
    {
        return $this->belongsToMany('App\Speciality');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }
}
