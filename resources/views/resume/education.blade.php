<h2 class="education"><?= mb_ucfirst(Lang::get('app.education')) ?></h2>

<form action="#">
    <fieldset>
        <div class="row-columns education_item">
            <div class="col">
                <div class="input-holder">
                    <?= Form::text('education[university]',
                            '',
                            ['placeholder' => mb_ucfirst(Lang::get('app.university')),
                                    'class' => 'form-control education_university',
                            ]);
                    ?>
                </div>
            </div>
            <div class="col">
                <div class="input-holder">
                    <?= Form::text('education[city]',
                            '',
                            ['placeholder' => mb_ucfirst(Lang::get('app.city')),
                                    'class' => 'form-control education_city',
                            ]);
                    ?>
                </div>
            </div>
            <div class="col">
                <div class="input-holder">
                    <?= Form::text('education[year]',
                            '',
                            ['placeholder' => mb_ucfirst(Lang::get('app.year')),
                                    'class' => 'form-control education_year',
                            ]);
                    ?>
                </div>
            </div>


            <div class="col">
                <a href="#" class="add add_education">+</a>
            </div>
        </div>
    </fieldset>
</form>