$(document).ready(function() {

    $('#send_resume').on('click', function(event) {
        var id = $(event.currentTarget).attr('data-id');

        $.ajax({
            url:    '/api/v1.0/resume/send/' + id,
            type:   'GET',
            success: function(data) {
                if ( data.success === true ) {
                    $('.popup').removeClass('hidden');

                    var intervalId = window.setInterval(function() {
                        window.scrollTo(0, window.pageYOffset - 10);

                        if ( window.pageYOffset === 0 ) {
                            clearInterval(intervalId);
                        }
                    }, 5);

                } else {
                    alert('Error while sending Your resume. Please try again');
                }
            }
        });

        return false;
    });

    $('#close_sent_popup').on('click', function() {
        $('.popup').addClass('hidden');

        return false;
    });
});