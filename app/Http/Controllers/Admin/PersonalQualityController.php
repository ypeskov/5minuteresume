<?php
/**
 * User: Yuriy Peskov<yuriy.peskov@gmail.com>
 * Date: 2015-06-17
 * Time: 07:37
 */

namespace App\Http\Controllers\Admin;

use App\PersonalQualityTag;


class PersonalQualityController extends BaseItemController
{
    public function indexAction()
    {
        $data['tags'] = PersonalQualityTag::all()->toArray();

        return view('admin.personal-quality.personal-quality-index', ['data' => $data]);
    }
}